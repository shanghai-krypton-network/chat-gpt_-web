# chatGPT_Web

#### 介绍
基于nodejs搭建网页版的chatGPT网页聊天

#### 软件架构
基于nodejs开发的前后端，都是用简单的js技术进行编写，易懂易开发


#### 安装教程

## 前端
1.  安装依赖  npm install
2.  项目启动  npm run dev
3.  项目打包  npm run build

修改script.js 112行
  const response = await fetch('http://后端地址 也就是server启动地址', {
        method: 'POST',
![修改后台地址](client/assets/image.png)

## 后端
1.  安装依赖 npm install
2.  项目启动 npm run
3.  无需打包


#### 使用说明

1.  后端需要修改 openAI 的key，在后端server项目中的 .env
    OPENAI_API_KEY=sk-xxxx

## 网页预览
http://chat.wxredcover.cn/